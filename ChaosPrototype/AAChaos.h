//
//  AAChaos.h
//  ChaosPrototype
//
//  Created by GW on 10/7/12.
//  Copyright (c) 2012 Algorhythm_Audio. All rights reserved.
//

#ifndef __ChaosPrototype__AAChaos__
#define __ChaosPrototype__AAChaos__

#include <iostream>


typedef struct chaosVars {
    double x, y, z;
} ChaosVars;


class AAChaos {
    double      p, r, b; // user can change these values
    double      scale;
    
public:
    ChaosVars   *chaos;
    
    AAChaos();
    ~AAChaos();
    
    void chaosTick();
    
    // Getters
    ChaosVars*      getChaos()  {return chaos;}
    double          getPrandt() {return p;}
    double          getRayleigh() {return r;}
    double          getConstant() {return b;}
    double          getScale() {return scale;}
    
    // Setters
    void            setPrandt       (double prandt);
    void            setRayleigh     (double rayleigh);
    void            setConstant     (double constant);
    void            setAllParams    (double prandt, double rayleigh, double constant);
    void            setScale        (double thisScale);
    
    // Other
    void            printChaos();
    
    
private:
    
    double dx, dy, dz;
    
    
};



























#endif /* defined(__ChaosPrototype__AAChaos__) */






