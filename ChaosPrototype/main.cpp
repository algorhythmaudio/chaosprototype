#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "portsf.h"

#include "AAChaos.h"
#include "AADistortion.h"
#include "AAButterworthFilter.h"
#include "AAIIRFilter.h"
#include "AABasic.h"





// portsf macros
#define kSampleRate 44100
#define kOutFile "chaosTesting.wav"
#define kNumFrames 4096

// chaos macros
#define kX 0.1
#define kY 0.1
#define kZ 0.1
#define kParandt 10.0
#define kRayleigh 28.0
#define kConstant 2.67
#define kScale 0.01
#define kRate 0.25 // in seconds

// filter macros
#define kBypassFilter 0
#define kQ 5.6
#define kOrder 1

// Distortion macros
#define kBypassDistortion 0
#define kDistMult 0.2

// Panning macros
#define kBypassPan 0




enum  {
    kInfile = 1,
    numArgs
};








int main(int argc, const char * argv[]) {
    // general vars
    int                 err = 0;
    float               val;
    unsigned long int   samp, overage[2];
    
    // chaos vars
    AAChaos             *mod;
    int                 j=0;
    int                 chaosRate;
    
    // filter vars
    AAButterworthFilter *filterLeft;
    AAButterworthFilter *filterRight;
    
    // portsf variables
    PSF_PROPS           propsIn, propsOut;
    int                 ifd = -1, ofd = -1;
    float               *frames = NULL;
    unsigned int        framesRead = 0;
    unsigned long       blockSize;
    clock_t             timeStart, timeEnd;
    int                 i;
    
    
    puts("\n\nChaosPrototype");
    
        
    
    
    // check arguments
    if (argc != numArgs) {
        printf("Insufficient number of aruments.\n"
               "Usage:\n"
               "\tinFile = stereo audio file to be manipulate\n");
        
        return 1;
    }
    
    
    
    
    
    
    
    // portsf init
    if (psf_init()) {
        puts("unable to start psf");
        err++;
        goto myexit;
    }
    
    ifd = psf_sndOpen(argv[kInfile], &propsIn, 0);
    if (ifd < 0) {
        printf("Error opening inFile %s\n", argv[kInfile]);
        printf("ifd = %i\n", ifd);
        err++;
        goto myexit;
    }
    
    if (propsIn.chans != 2) {
        puts("Error: inFile must be stereo");
        err++;
        goto myexit;
    }
    
    propsOut = propsIn;
    propsOut.format = psf_getFormatExt(&argc[kOutFile]);
    if (propsOut.format == PSF_FMT_UNKNOWN) {
        printf("outfile name has unknown format.\n"
               "Use any of .wav, .aiff, .aif, .afc, .aifc\n");
        err++;
        goto myexit;
    }

    
    
    
    ofd = psf_sndCreate(kOutFile, &propsOut, 0, 0, PSF_CREATE_RDWR);
    if (ofd < 0) {
        puts("Error: couldn't create the outfile");
        printf("ofd = %i", ofd);
        err++;
        goto myexit;
    }
    
    
    
    
    
    
    // chaos init
    mod = new AAChaos;
    if (mod == NULL) {
        puts("Error: counldn't create mod");
        err++;
        goto myexit;
    }
    
    mod->setAllParams(kParandt, kRayleigh, kConstant);
    mod->setScale(kScale);
    
    mod->chaos->x = (double) kX;
    mod->chaos->y = (double) kY;
    mod->chaos->z = (double) kZ;
    
    chaosRate = (int) (kSampleRate * kRate);
    j = chaosRate;
    
    
    
    
    
    
    
    
    // filter init
    filterLeft = new AAButterworthFilter(FILTER_HP, kOrder, kSampleRate, CHAN_MONO);
    filterRight = new AAButterworthFilter(FILTER_HP, kOrder, kSampleRate, CHAN_MONO);
    filterLeft->setQ(5.6);
    filterRight->setQ(5.6);
    
    
    
    
    
    
    
    
    
    // alloc memory
    frames = (float*) malloc(sizeof(float) * kNumFrames * propsIn.chans);
    if (frames == NULL) {
        puts("Error: couldn't alloc memory for framesIn");
        err++;
        goto myexit;
    }
    
    
    
    
    
    
    
    
    // prep
    framesRead = psf_sndReadFloatFrames(ifd, frames, kNumFrames);
    mod->chaosTick();
    samp = 0;
    overage[0] = 0;
    overage[1] = 0;
        
    
    
    
    
    
    
    
    
    // DSP
    puts("Processing...");
    timeStart = clock();
    
    while (framesRead > 0) {
        blockSize = framesRead * propsIn.chans;
        
        // proc filter loop
        for (i=0; i<blockSize; i+=2, j-=2, samp += 2) {
        
            // scale input signal to prevent clipping
            frames[i] *= 0.9;
            frames[i+1] *= 0.9;
            
            // calculate chaos params
            if (j <= 0) {
                j=chaosRate;
                mod->chaosTick();
                filterLeft->setFrequency((mod->chaos->y*mod->chaos->y) * 16.0);
                filterRight->setFrequency((mod->chaos->y*mod->chaos->y) * 16.0);
                printf("freq: %.1f\n", filterLeft->getFrequency());
                //printf("dist: %.2f\n", mod->chaos->z * kDistMult);
                //mod->printChaos();
            }
            
            
            // filter proc
            if (!kBypassFilter) {
                filterLeft->process(&frames[i], &frames[i], 1);
                filterRight->process(&frames[i+1], &frames[i+1], 1);
            }
        
            
            // proc distortion
            if (!kBypassDistortion ) {
                val = waveShaper1(frames[i], static_cast<float>(mod->chaos->z * kDistMult));
                frames[i] = val;
                val = waveShaper1(frames[i+1], static_cast<float>(mod->chaos->z * kDistMult));
                frames[i+1] = val;
            }
            
            
            
            // proc pan
            if (!kBypassPan) {
                val = mod->chaos->x / 18.0;
            
                // this if statement can be simplefied: see Hackers Delight pg 68
                // a<=x<=b is abs(x-a) <= abs(b-a)
                if (fabsf(val - -1.0) <= 2.0) {
                    const float angle = (90.0 * val) * 0.5;
                    frames[i] *= ROOT2OVR2 * (cosf(angle) - sinf(angle)); // left
                    frames[i+1] *= ROOT2OVR2 * (cosf(angle) + sinf(angle)); // right
                }
                else if (val > 1.0)
                    frames[i] = 0;
                else
                    frames[i+1] = 0;
            }
            
            if (fabsf(frames[i] > 1))
                overage[0]++;
            if (fabsf(frames[i+1] > 1))
                overage[1]++;
            
        }
        
        
        
        if (psf_sndWriteFloatFrames(ofd, frames, framesRead) != framesRead) {
            puts("Error: couldn't write frames to outFile");
            err++;
            goto myexit;
        }
        
        framesRead = psf_sndReadFloatFrames(ifd, frames, kNumFrames);
    }
    
    
    
    
    
    
    
    
    
    
    // wrap it up
    timeEnd = clock();
    printf("Complete with %d errors\n", err);
    printf("Elapsed time = %f secs\n", (timeEnd-timeStart) / (double)CLOCKS_PER_SEC);
    
    printf("overage left: %li\n", overage[0]);
    printf("overage right: %li\n", overage[1]);
    
    puts("done.\n");

    
    
    
    
    
    
    
    
    
myexit:
    if (ifd > 0) psf_sndClose(ifd);
    
    if (ofd >= 0)
        if (psf_sndClose(ofd))
            printf("Error closing outFile: %s\n", kOutFile);
    
    if (frames)    free(frames);
    if (mod)    delete mod;
    if (filterLeft) delete filterLeft;
    if (filterRight) delete filterRight;
    
    psf_finish();
    return err;
}

















