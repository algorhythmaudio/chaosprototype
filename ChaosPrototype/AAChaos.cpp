#include "AAChaos.h"



#pragma mark Life


AAChaos :: AAChaos() {
    chaos = (ChaosVars*) malloc(sizeof(ChaosVars));
    if (chaos == NULL) {
        puts("Couldn't init AAChaos vaiables");
        this->~AAChaos(); // is this right???
    }
    else {
        puts("AAChaos init");
        
        // set ivars
        p = 10.0;
        r = 28.0;
        b = 2.67;
        scale = 0.01;
        
        chaos->x = 0.1;
        chaos->y = 0.1;
        chaos->z = 0.1;
    }
}


AAChaos :: ~AAChaos() {
    if (chaos) free(chaos);
}





#pragma mark - Setters



void AAChaos :: setPrandt (double prandt) {
    if (prandt <= 0) return;
    p = prandt;
}


void AAChaos :: setRayleigh (double rayleigh) {
    if (rayleigh <= 0) return;
    r = rayleigh;
}


void AAChaos :: setConstant (double constant) {
    if (constant <= 0) return;
    b = constant;
}


void AAChaos :: setAllParams (double prandt, double rayleigh, double constant) {
    if (prandt <=0 || rayleigh <= 0 || constant <= 0) return;
    
    p = prandt;
    r = rayleigh;
    b = constant;
}


void AAChaos :: setScale (double thisScale) {
    if (thisScale <= 0) return;
    scale = thisScale;
}








#pragma mark - Other

void AAChaos :: printChaos() {
    printf("x = %f ", chaos->x);
    printf("y = %f ", chaos->y);
    printf("z = %f\n", chaos->z);
}








#pragma mark - Generators


void AAChaos :: chaosTick() {
    dx = p * (chaos->y - chaos->x);
    dy = (r*chaos->x) - chaos->y - (chaos->x * chaos->z);
    dz = (chaos->x * chaos->y) - (b * chaos->z);
    
    chaos->x += dx*scale;
    chaos->y += dy*scale;
    chaos->z += dz*scale;
}


























